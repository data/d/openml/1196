# OpenML dataset: BNG(pharynx)

https://www.openml.org/d/1196

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Please cite**:   

Juan J. Rodriguez, Ludmila I. Kuncheva, Carlos J. Alonso (2006). Rotation Forest: A new classifier ensemble method. IEEE Transactions on Pattern Analysis and Machine Intelligence. 28(10):1619-1630. URL http://doi.ieeecomputersociety.org/10.1109/TPAMI.2006.211.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1196) of an [OpenML dataset](https://www.openml.org/d/1196). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1196/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1196/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1196/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

